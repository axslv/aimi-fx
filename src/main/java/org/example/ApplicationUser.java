package org.example;

public class ApplicationUser {
    public String username;
    public String password;

    public ApplicationUser(String username, String password) {
        this.password = password;
        this.username = username;
    }

}
