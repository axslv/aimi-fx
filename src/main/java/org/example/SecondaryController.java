package org.example;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class SecondaryController {

    @FXML
    private TextArea editor;

    @FXML
    private void switchToPrimary() throws IOException {
        String text = editor.getText();
        App.setRoot("primary");
    }
}