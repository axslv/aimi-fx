package org.example;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.example.service.LoginService;

public class PrimaryController {

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button loginButton;

    @FXML
    private void login() throws IOException {

        boolean login = LoginService.login(usernameField.getText(), passwordField.getText());

        if (login) {
            App.setRoot("secondary");
        } else {
            loginButton.setText("Error!");
        }


    }



}
