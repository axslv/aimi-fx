package org.example.service;

import org.example.ApplicationUser;
import org.example.stub.UsersStorage;

public class LoginService {

    public static boolean login(String username, String password) {
        return UsersStorage.checkLogin(new ApplicationUser(username, password));
    }

}
