package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


/*
TODO: Create class model/Document with fields: documentId (String), documentNumber (String),
  issuer (String), issueDate (Date), expiresOn (Date).

TODO: Create field of type List<Document> in ApplicationUser class

TODO:  Create class model/Signature with fields: signedBy (String), signature (String)

TODO: Create interface model/Signable, add method signature for void sign() method.

TODO: make Document implement Signable interface
 */

public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 640, 480);
        stage.setScene(scene);
        stage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}