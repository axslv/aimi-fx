package org.example.stub;

import org.example.ApplicationUser;

import java.util.List;
import java.util.stream.Collectors;

public class UsersStorage {
    public static List<ApplicationUser> applicationUsers = List.of(new ApplicationUser("max317", "aaa"));

    public static void save() {
        //TODO: implement save

    }

    //This look through application users list, finds user by username and password
    //and reports if user is found
    public static boolean checkLogin(ApplicationUser user) {
        return !applicationUsers.stream()
                .filter(appUser -> user.username.equalsIgnoreCase(appUser.username))
                .filter(appUser -> user.password.equals(appUser.password))
                .collect(Collectors.toList()).isEmpty();
    }

}
